# Niurka samples repo

![Niurka Marcos](niurka.jpg "Niurka Marcos")

## About this repo

This repo intends to collect audio samples of Niurka Marcos for sampling, Dj's, etc;
we deeply hope Niurka is not mad about this.


## TO DO

- Transcribe audio to text.
- Chop audios.
- Order samples
- Download more audios
- Make remix examples.

